# Checkov IaC SAST Scanning

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/ci-components/checkov-iac-sast)

## Usage

### Vendor Documentation

[Vendor Documentation](https://www.checkov.io/1.Welcome/Quick%20Start.html)

### Inputs and Configuration

| Input | Default value |    Type     | Description |
| ----- | ------------- | ----------- | ----------- |
| `CKV_CONTAINER_TAG` | (see ci file) | CI Component Input     | Container tag to peg to. |
| `stage` | test | CI Component Input     | The CI Stage to run the component in. |
| `CI_DEBUG_TRACE` | N/A | CI Variable     | Causes verbose component output for debugging. Set for pipeline or specific job. |
| `CKV_IS_ALLOW_FAILURE` | N/A | CI Variable     | Whether the component fails builds upon findings. It is not GitLab best practice for an individual scanner job to fail the build. Search documentation about Security Policy Merge Approval rules. |
| `CKV_ATTEMPT_COLOR_LOGS` | N/A | CI Variable     | Attempt to use the linux script command to create color in GitLab CI logs. |

### Smart Switching Between Security Dashboards and JUNIT Visualization

If you have GitLab Ultimate, the results will be treated like any other GitLab scanner and be visible in Security Dashboards, Merge Requests and available for Security Policy Merge Approvals.

If you have a lower license, the results will be available in JUNIT test visualizations in your pipeline results.

### Including

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: $CI_SERVER_FQDN/guided-explorations/ci-components/checkov-iac-sast/checkov-iac-sast@<VERSION>
```
See "Components" tab in CI Catalog to copy exactly point to the current version.

where `<VERSION>` is the latest released tag or `main`.

### Original Canonical Source Code

The original source for this component is at: https://gitlab.com/guided-explorations/ci-components/checkov-iac-sast

### Validation Test

If you simply include the component and every job after it will have the following pipeline variables that used the infamous GitVersion to create a unique SemVer according
to complex uniqueness rules that are configurable by you in the GitVersion.yml file.

### Working Example Code Using This Component

- [Checkov IaC SAST Working Example](https://gitlab.com/guided-explorations/ci-cd-plugin-extensions/checkov-iac-sast) - be sure to check the merge requests to see the [findings in an MR](https://gitlab.com/guided-explorations/ci-cd-plugin-extensions/checkov-iac-sast/-/merge_requests).

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components